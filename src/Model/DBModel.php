<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing data in a MySQL database using PDO.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */

require_once("AbstractModel.php");
require_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @todo implement class functionality.
 */
class DBModel extends AbstractModel
{
    protected $db = null;
    
    /**
     * @param PDO $db PDO object for the database; a new one will be created if no PDO object
     *                is passed
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function __construct($db = null)
    {
        if ($db) 
        {
            $this->db = $db;
        } 
        else {
            $db = new PDO('mysql:host=localhost;dbname=test;charset=utf8','group7','password', 
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            }
           
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookList()
    {
        $booklist = array();
        $sql = 'SELECT * FROM book';

        foreach ($this->db->query($sql) as $row) {
            $booklist[] = new Book($row['title'], $row['author'], $row['description'], $row['id']);
        }
            return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookById($id)
    {
            self::verifyId($id);
                $stmt = $this->db->prepare("SELECT * FROM book WHERE id=?");
                $stmt->execute(array($id));
                $info = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($info != "") {
                    $book = new Book($info['title'], $info['author'], $info['description'], $info['id']);
                    return $book;
                }
}
    /** Adds a new book to the collection.
     * @param Book $book The book to be added - the id of the book will be set after successful insertion.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function addBook($book)
    {
            self::verifyBook($book, true);
			$stmt = $this->db->prepare("INSERT INTO book (title, author, description) VALUES (?, ?, ?)");
			$stmt->execute(array($book->title, $book->author, $book->description));
            $book->id = $this->db->lastInsertId();
    }

    /** Modifies data related to a book in the collection.<
     * @param Book $book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function modifyBook($book)
    {
        
		$stmt = $this->db->prepare("UPDATE book SET title=?, author=?, description=? WHERE id=?");
        $stmt->bindValue(1, $book->title, PDO::PARAM_STR);
        $stmt->bindValue(2, $book->author, PDO::PARAM_STR);
        $stmt->bindValue(3, $book->description, PDO::PARAM_STR);
        $stmt->bindValue(4, 1, PDO::PARAM_STR);
		$stmt->execute();
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function deleteBook($id)
    {
        self::verifyId($id);
        $stmt = $this->db->prepare("DELETE FROM book WHERE id=?");
        $stmt->bindValue(1, $id, PDO::PARAM_INT);
        $stmt->execute();
    }
}
